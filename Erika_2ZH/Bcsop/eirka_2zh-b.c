#include <stdlib.h>
#include <stdio.h>
#include <time.h>

void feltolt(int* array, int n); 
void kiir(int* array, int n);
void intervallum(int* a, int *b);
int intervallumElemek(int* array, int* a, int* b, int n);


int main()
{
    int n = 6; //tömb elemszám
    int a, b;
    int array[n]; //deklaráljon egy 6 elemű egész számos tömböt
    feltolt(array, n);
    kiir(array, n);
    intervallum(&a,&b);
    int x = intervallumElemek(array, &a,&b, n);
    double szazalek = ((double)x/n)*100;
    printf("az elemek %.2f szazaleka esik a tartomanyba.\n",szazalek);

    return 0;
}

void feltolt(int* array, int n) 
{
    /* töltse fel 100-1000 között random számokkal a tömbböt*/
    srand(time(0)); //random inicializálás

    for(int i = 0; i < n; i++)
    {
        array[i]= rand() % 900 + 100; //900+100 a max, +100 onnan indul
    }
}

void kiir(int* array, int n)
{
    printf("a tomb elemei: \n");
    for (int i = 0; i < n; i++)
    {
        printf("%d\n",array[i]);
    }
    
}

void intervallum(int* a, int *b)
{
    //olvass be 2 számot ellenőrzötten 100 - 100 között
    printf("adjad meg az intervallumot! (100 - 1000)\n");
    int ok;
    do
    {
        ok = 0;
        if(scanf("%d %d",a,b) != 2)// a scanf intet ad vissza valamiért annyi értékkel ahány darab adatot beolvasott.
        {//%d-t olvasunk be ezért nem kell nézni hogy betűt ad e meg az ember.
            ok = 1;
            printf("hiba van\n");
            while (getchar() != '\n'); //getchar() karaktert olvas be az input bufferből ezért '' ilyenek kellenek
        }
        //meg kell vizsgálni, hogy a beolvasott számok értelmesek-e
        if(*a < 100 || *a > 1000 || *b < 100 || *b > 1000) //ne legyen kisebb 100nál vagy több mint 1000 
        {
            printf("az intervallum a tomb hataran tul mutat!\nProbald ujra\n");
            ok = 1;
        }
        if(*a >= *b || *b <= *a) //a-nak kisebbnek kell lennie b-től
        {
            printf("hibas intervallum hatarok!!\nProbald ujra\n");
            ok = 1;
        }
    } while (ok != 0);
    
}
int intervallumElemek(int* array, int* a, int* b, int n)
{
    int bennevan = 0;//tartományba eső elmek száma. utolsó feladathoz kell
    //írja ki a tartományba eső elemeket
    for(int i = 0; i < n; i++)
    {
        if(array[i] >= *a && array[i] <= *b)
        {
            printf("%d\n",array[i]);
            bennevan ++;
        }
    }
    return bennevan;
}