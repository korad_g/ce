#include <stdlib.h>
#include <stdio.h>
#include <time.h>

void feltolt(int* array, int n); 
void kiir(int* array, int n);
void intervallum(int* a, int *b);
void intervallumElemek(int* array, int* a, int* b);
int intervallumMax(int* array, int* a, int* b);

int main()
{
    int n = 6; //tömb elemszám
    int a, b;
    int array[n]; //deklaráljon egy 6 elemű egész számos tömböt
    feltolt(array, n);
    kiir(array, n);
    intervallum(&a,&b);
    //printf("%d %d\n",a,b);
    intervallumElemek(array,&a,&b);//sima változóknál ha pointer van a fv-ben kell az &, az fv-ben meg a * a változó nevéhez
    int maximum = intervallumMax(array, &a, &b);//van visszatérés el kell menteni
    printf("a legnagyobb elem az intervallumban: %d\n",maximum);
    return 0;
}

void feltolt(int* array, int n) 
{
    /* töltse fel 1-100 között random számokkal a tömbböt*/
    srand(time(0)); //random inicializálás

    for(int i = 0; i < n; i++)
    {
        array[i]= rand() % 99 + 1; //100 a max, +1 onnan indul 
                                    //ha jól tudom így a 100 nem lesz benne. nem biztos
    }
}

void kiir(int* array, int n)
{
    printf("a tomb elemei: \n");
    for (int i = 0; i < n; i++)
    {
        printf("%d\n",array[i]);
    }
    
}

void intervallum(int* a, int *b)
{
    //olvass be 2 számot ellenőrzötten 0-5 között
    printf("adjad meg az intervallumot! (0-5)\n");
    int ok;
    do
    {
        ok = 0;
        if(scanf("%d %d",a,b) != 2)// a scanf intet ad vissza valamiért annyi értékkel ahány darab adatot beolvasott.
        {//%d-t olvasunk be ezért nem kell nézni hogy betűt ad e meg az ember.
            ok = 1;
            printf("hiba van\n");
            while (getchar() != '\n'); //getchar() karaktert olvas be az input bufferből ezért '' ilyenek kellenek
        }
        //meg kell vizsgálni, hogy a beolvasott számok értelmesek-e
        if(*a < 0 || *a > 5 || *b < 0 || *b > 5) //ne legyen negatív vagy több mint 5 
        {
            printf("az intervallum a tomb hataran tul mutat!\nProbald ujra\n");
            ok = 1;
        }
        if(*a >= *b || *b <= *a) //a-nak kisebbnek kell lennie b-től
        {
            printf("hibas intervallum hatarok!!\nProbald ujra\n");
            ok = 1;
        }
    } while (ok != 0);
    
}

void intervallumElemek(int* array, int* a, int* b)
{
    //írja ki az intervallumba eső elemeket
    printf("az intervallumba eső elemek: \n");
    for(int i = *a; i <= *b; i++)//a-tól indul b-ig megy
    {
        printf("%d, ",array[i]);
    }
    printf("\n");
}

int intervallumMax(int* array, int* a, int* b)
{
    //intervallumban a legnagyobb elem
    //max elem kiválasztás
    int max = 0;
    for(int i = *a; i <= *b; i++)
    {
        //ha megfordítod a relációt akkor minimum kiválasztás
        if(array[i] > max)
        {
            max = array[i];
        }
    }
    return max;
}