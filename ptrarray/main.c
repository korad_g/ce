#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <stdbool.h>
#include <ctype.h>

#define SIZE 20
#define STR_LEN 255

void feltolt(int (*ptr)[]);
void kiir(int (*ptr)[]);
void novekvo(int (*ptr)[]);
void osszeg(int (*ptr)[]);
void szorzat(int (*ptr)[]);
void max(int (*ptr)[]);
void min(int (*ptr)[]);
void strbeolvas(char (*ptr)[]);
void strkiir(char (*ptr)[]);
void strvisszafele(char (*ptr)[], char (*out)[]);
int mennyiebetu(char (*ptr)[] , char *ch);
char charbeolvas();


int main()
{
    int array[SIZE];
    feltolt(&array);
    kiir(&array);

    osszeg(&array);
    szorzat(&array);
    max(&array);
    min(&array);

    novekvo(&array);
    printf("rendezes utan: \n");
    kiir(&array);

    char string[STR_LEN];
    strbeolvas(&string);
    strkiir(&string);
    char rev[STR_LEN];
    strvisszafele(&string,&rev);
    printf("string visszafele:\n");
    strkiir(&rev);
    char c = charbeolvas();
    mennyiebetu(&string, &c);

    return 0;
}

void feltolt(int (*ptr)[])
{
    bool ok;
    srand(time(0));
    
    for(int i = 0; i < SIZE; i++)
    {
        (*ptr)[i] = rand() % 200 + 1;

            ok = true;
            for(int j = 0; j < i; j++)
            {
                if((*ptr)[j] == (*ptr)[i]){
                    ok = false;
                    i--;
                    break;
                }
            }
    }
}

void kiir(int (*ptr)[])
{
    for(int i = 0; i < SIZE; i++)
    {
        printf("%d ",(*ptr)[i]);
    }
    printf("\n");
}

void novekvo(int (*ptr)[])
{
    int temp;
    for(int i = 0; i < SIZE; i++)
    {
        for(int j = 0; j < SIZE; j++)
        {
            if((*ptr)[j] > (*ptr)[i])
            {
                temp = (*ptr)[i];
                (*ptr)[i] = (*ptr)[j];
                (*ptr)[j] = temp;
            }
        }
    }
}

void osszeg(int (*ptr)[])
{
    int num = 0;
    for(int i = 0; i < SIZE; i++)
    {
        num += (*ptr)[i];
    }
    printf("A szamok osszege: %d\n",num);
}

void szorzat(int (*ptr)[])
{
    int num = 1;
    for(int i = 0; i < SIZE; i++)
    {
        num *= (*ptr)[i];
    }
    printf("A szamok szorzata: %d\n", num);
}

void max(int (*ptr)[])
{
    int maxindex=0;
    for (int i = 0; i < SIZE; i++)
    {
        if((*ptr) [i] > (*ptr)[maxindex])
        {
            maxindex=i;
        }
    }
    printf("a legnagyobb elem: %d, indexe: %d\n", (*ptr)[maxindex], maxindex+1);
}

void min(int (*ptr)[])
{
    int minindex=0;
    for (int i = 0; i < SIZE; i++)
    {
        if((*ptr) [i] < (*ptr)[minindex])
        {
            minindex=i;
        }
    }
    printf("a legkisebb elem: %d, indexe: %d\n", (*ptr)[minindex], minindex+1);
}

void strbeolvas(char (*ptr)[])
{
    printf("Irjal be valami szart\n");
    scanf("%[^\n]",&(*ptr));
}

void strkiir(char (*ptr)[])
{
    printf("string: %s\n",(*ptr));
}

void strvisszafele(char (*ptr)[], char (*out)[])
{
    for(int i = 0; i < strlen(*ptr); i++){
        (*out)[strlen(*ptr) - i - 1] = (*ptr)[i];
    }
}

int mennyiebetu(char (*ptr)[] , char *ch)
{
    int count = 0;
    for (int i = 0; i < strlen(*ptr); i++){
        if(tolower((*ptr)[i]) == *ch){
            count++;
        }
    }
    return count;
}

char charbeolvas()
{
    printf("milyen betut keressek?\n");
    char ch;
    bool ok;
    int input;
    do
    {
        ok = true;

        input = scanf("%c",&ch);
        printf("%c\n", ch);
        if(input != 1)
        {
            ok = false;
            printf("hibas input!\n");
            while(getchar() != '\n');
        }

        if(isdigit(ch) == 1)
        {
            printf("szamot adtal meg!\n");
            while(getchar() != '\n');
            ok = false;
        }
        
        
    } while (!ok);
    return ch;
}