#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define PI 3.14
int main()
{
    double r;
    printf("Kerem a kor sugarat! (cm)\n");
    scanf("%lf",&r);
    if(r<0){
        r *= -1;
    }
    if(r==0){
        printf("ERROR \n");
        return 1;
    }
    printf("a kor terulete: %lfcm2\n",(r*r)*PI);
    return 0;
}