#include<stdio.h>
#include<stdlib.h>
#include<math.h>

int main(){

    int N, i=1, tipus; //N=hívasok száma; tipus: 1-külföldi, 2-hálózaton belül 3-hálózton kívül
    double ido, sum_kf, sum_hk, sum_hb;
    sum_hb=sum_hk=sum_kf=0;
    printf("Mennyi adat lesz? ");
    scanf("%d",&N);

    while (i<=N)
    {
        printf("%d hivas adatai: \n",i);
        printf("Tipus [1-kulfoldi, 2-halozaton belul 3-halozton kivul]");
        scanf("%d",&tipus);
        printf("Beszelgetes ideje (percben): ");
        scanf("%lf",&ido);
        if(tipus == 1){
            sum_kf = sum_kf+(ido*100);
        }
        else{
            if(tipus==2){
                sum_hk=sum_hk+(ido*60);
            }
            else{
                if(tipus==3){
                    sum_hb=sum_hb+(ido*40);
                }
            }
        }
        i++;
    }
    
    printf("\n A szamla vegosszege: %.2f(1) + %.2f(2) + %2f(3) = %.2fFt\n",sum_kf,sum_hk,sum_hb,sum_kf+sum_hk+sum_hb);


    return 0;
}