#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#define STR_LEN 255

typedef struct bday
{
    unsigned int year;
    unsigned int month;
    unsigned int day;
}Bday;


typedef struct employee
{
    unsigned int id;
    char name[STR_LEN];
    Bday date_of_birth;
    unsigned int hours;
    double salary;
}Employee;

Employee emp_in();
void emp_out(Employee emp);
int readu();
void emp_salary_doubler(Employee *emp);
int highest_salary(Employee emp[], unsigned int size);
Bday bday_reader();
void date_out(Bday *date);

int main()
{
    //Employee emp;
    printf("number of empoyees: ");
    unsigned int empnum = readu();
    printf("\n");
    Employee emps[empnum];

    for(int i = 0; i < empnum; i++)
    {
        emps[i] = emp_in();
        emp_salary_doubler(&emps[i]);
    }
  
    printf("employee data\n");
    printf("ID\tName\tdate of birth\tHours\tSalary\n");
    for(int i = 0; i < empnum; i++)
    {
        emp_out(emps[i]);
    }

    printf("employee with the highest salary: \n");
    emp_out(emps[highest_salary(emps,empnum)]);
    return 0;
}

Employee emp_in()
{
    Employee emp;
    printf("Employee data: \n");
    bool ok;
    printf("ID:\n");
    emp.id=readu();
    printf("NAME:\n");
    scanf(" %[^\n]",emp.name);
    printf("Date of birth:\n");
    emp.date_of_birth=bday_reader();
    printf("HOURS:\n");
    emp.hours=readu();
    printf("SALARY:\n");
    emp.salary=(double)readu();
    return emp;
}

void emp_out(Employee emp)
{
    printf("%u\t%s\t%u:%u:%u\t%u\t%.2f$\n", emp.id, emp.name, emp.date_of_birth.year,emp.date_of_birth.month,emp.date_of_birth.day, emp.hours, emp.salary);
}

int readu()
{
    bool ok;
    int input;
    do
    {
        ok = true;
        if(scanf("%d",&input) != 1)
        {
            ok = false;
            while(getchar() != '\n');
        }
    } while (!ok);
    return input;
    
}

void emp_salary_doubler(Employee *emp)
{
    emp->salary *= 2;
}

int highest_salary(Employee emps[], unsigned int size)
{
    unsigned int max = 0;
    unsigned int maxindex = 0;
    for(unsigned int i = 0; i < size; i++)
    {
        if(emps[i].salary > max)
        {
            max = emps[i].salary;
            maxindex = i;
        }
    }
    return maxindex;
}

Bday bday_reader()
{
    Bday date;
    printf("Year:\n");
    date.year=readu();
    printf("Month:\n");
    date.month=readu();
    printf("Day:\n");
    date.day=readu();
    return date;
}

