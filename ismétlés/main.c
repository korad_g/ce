#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void feltolt(int* array, int n);
void kiir(int* array, int n);
int intervallum();
int nagyobb(int* array, int* n, int* a);

int main()
{
    int n = 5;
    int array[n];

    feltolt(array, n);
    kiir(array, n);
    printf("adjon meg egy tomb indexet (0-4)\n");
    int a = intervallum();
    int nagy = nagyobb(array, &n, &a);
    printf("a tombben %ddb elem nagyobb mint %d\n",nagy, array[a]);
    return 0;
}

void feltolt(int* array, int n)
{
   srand(time(0));
   for(int i = 0; i < n; i++)
   {
    array[i] = rand() % 10 + 1; //a random bekerül éppen utolsó helyre
    for(int j = 0; j < i; j++) // j elindul a tömb elejétől
    {
        if(array[j] == array[i]) //ha talál az utolsóval egyező számot
        {
            i--;    //egyet visszalépteti az i-t
            break;  //kilép a belső forból
        }
            //amúgy mehet tovább rendesen 
    }
   }
}

void kiir(int* array, int n)
{
    for(int i = 0; i < n; i++)
    {
        printf("%d\n",array[i]);
    }
}

int intervallum()
{
    int a;
    int ok;
    do
    {
        ok = 0;
        if(scanf("%d",&a) != 1)
        {
            ok = 1;
            printf("Input Error!\n");
            while(getchar() != '\n');
        }

        if(a > 4 || a < 0)
        {
            printf("az index a tomb hatarain tul mutat!\n");
            ok = 1;
        }
    } while (ok != 0);
    return a;
}
int nagyobb(int* array, int* n, int* a)
{
    int nagy = 0;
    for(int i = 0; i < *n; i++)
    {
        if(array[i] > array[*a] && i != *a)
        {
            nagy++;
        }
    }
    return nagy;
}