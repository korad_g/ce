#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void kiir(float benzin[], float elektromos[]);
int beolvas();
void szazalek(float benzin[], float elektromos[], double num);


int main()
{
    const unsigned int tomb_meret = 5;
    float benzines[] = {6.3, 8.5, 7.3 ,7.5 ,8.1};
    float hibrid[] = {3.7, 4.7, 5.2, 5.3, 5.7};

    kiir(benzines,hibrid);
    double fogyasztas = beolvas();
    szazalek(benzines,hibrid,fogyasztas);
    return 0;
}

void kiir(float benzin[], float elektromos[])
{
    for(int i = 0; i < 5; i++)
    {
        printf("%.1f, %.1f\n",benzin[i],elektromos[i]);
    }
}

int beolvas()
{
    printf("ajd egy szamot 0.3 - 0.5 kozott\n");
    double szam=0;
    int ok;
    do
    {
       ok = 0;
       if(scanf("%lf",&szam) != 1)
       {
           printf("elba!\n");
           ok = 1;
           while (getchar() != '\n');

       }
    } while (ok != 0);
    return szam;
    
}
void szazalek(float benzin[], float elektromos[], double num)
{
    for (int i = 0; i < 5; i++)
    {
        double anyád= ((benzin[i]/elektromos[i])*100)-100;
        if(anyád > num*100)
        {
            printf("%.1f, ",anyád);
        }
    }
    printf("\n");
}