#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define MERET 10

void feltolt(int array[]);
void kiir(int array[]);
void intervallum(int *a, int *b);
int idoszak(int a, int b, int array[]);

int main()
{
    int array[MERET];
    feltolt(array);
    kiir(array);
    int a,b;
    intervallum(&a,&b);
    printf("a legnagyobb bevetel %d volt %d-kor",array[idoszak(a,b,array)], idoszak(a,b,array));
}

void feltolt(int array[])
{
    srand(time(NULL));
    for (int i = 0; i < MERET; i++)
    {
        array[i] = rand() % (85000 - 15000) + 15000;
    }
    
}

void kiir(int array[])
{
    for (int i = 0; i < MERET; i++)
    {
        printf("%d ",array[i]);
    }
    printf("\n");
}

void intervallum(int *a, int *b)
{
    printf("adj meg egy intervallumot (1-10)\n");
    int ok;
    do
    {
        ok = 0;
        if(scanf("%d %d",a,b) != 2 || *a <= 0 || *b <= 0 || *a > 10 || *b > 10)
        {
            printf("hibas input\n");
            ok = 1;
            while(getchar() != '\n');
        }

    } while (ok != 0);
    
}

int idoszak(int a, int b, int array[])
{
    int maxbev =0;
    for(int i = a; i <= b; i++)
    {
        if(array[i] > maxbev)
        {
            maxbev=i;
        }
    }
    return maxbev;
}